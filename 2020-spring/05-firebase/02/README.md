# 02. Firebase 데이터베이스
서비스에 사용되는 각종 데이터들을 관리

<br>

## 문서 둘러보기
* `Cloud Firestore`의 `웹` 항목 살펴보기

<br>

***

<br>

## 시작하기
> Firebase 콘솔에서 `Database` 탭 이동

<br>

### I. 데이터베이스 만들기
* 위치설정: 거주지가 속한 지역으로
* 규칙: `false`를 `true`로 변경

<br>

### II. 컬렉션 만들기
1. `컬렉션 시작`
  > * `my_info` 컬랙션 만들기
2. my_info의 필드와 값 각각 추가하기
  > * `like`와 `follow`는 비워둘 것
3. `data.js`의 `my_info` 값 변경
```javascript
var my_info = null;
```

<br>

### III. 사이트에서 데이터 접근

1. `body` 하단 Firebase 설정부 아래에 아래 코드들 추가
> 파이어베이스 기능 추가 (*버전 번호 모두 동일하게*)
```html
  <script src="https://www.gstatic.com/firebasejs/버전/firebase-firestore.js"></script>
```
> firestore 인스턴스 `db` 변수 지정
```javascript
  // Get a reference to the database service
  var db = firebase.firestore();
```

<br>

2. `init` 함수 변경

```javascript
function init () {
  loadMyInfo();
}

// my_info 를 받아오는 함수
function loadMyInfo () {
}
```

<br>

3. `my_info` 컬랙션의 데이터 받아와 보여주기
```javascript
function loadMyInfo () {
  db.collection("my_info").get().then(function (querySnapshot) {
    querySnapshot.forEach(function (doc) {
      my_info = doc.data();

      // 받아온 document의 id
      my_info.docId = doc.id;

      showMyInfo();
    })
  });
}
```

<br>

4. `my_info` 업데이트하기  
* 문서 하단의 `데이터 추가` 링크 참조

> DB 변경 함수 추가
```javascript
function updateMyInfoOnDB () {
  // ...
}
```
> 기존 저장 함수에 추가
```javascript
function updateMyInfo () {
  // ...
  // showMyInfo 대신
  updateMyInfoOnDB();
}
```
> DB 변경 함수 채워넣기
```javascript
function updateMyInfoOnDB () {
  db.collection("my_info").doc(my_info.docId).update({
    introduction: my_info.introduction,
    as: my_info.as,
    interest: my_info.interest
  }).then(function () {
    showMyInfo();
  })
}
```

<br>

***

<br>

## 다음 강좌  
* [03. Firebase 스토리지](https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring/05-firebase/03/README.md)