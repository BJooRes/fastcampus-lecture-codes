# 08. CSS Reset & 실습과제 

## CSS Reset
> 브라우저마다 다른 CSS 속성들을 초기화  
> * <a href="https://cssreset.com/scripts/eric-meyer-reset-css/" target="_blank">Eric Meyer's CSS Reset 2.0</a>

```css
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
```

<br>
<br>

***

<br>
<br>

## 최종과제
### <a href="https://fastcampus-css-final.netlify.com/" target="_blank">[사이트 보러가기]</a>

* 이제까지 배운 내용들을 활용해서 위 링크의 사이트를 똑같이 만들어보세요.  
* 크롬의 개발자 도구를 활용해서 배경색, 이미지 링크 등을 찾아 사용합니다.
* 구현 방법은 다양합니다.  여러가지를 시도하고 최선의 방식을 찾아가세요.

> *  <a href="https://gitlab.com/junseol86/fastcampus-lecture-codes/-/tree/master/2020-spring%2F03-css%2F08%2Ffinal" target="_blank">원본 코드와 파일들 [최종 참고용]</a>